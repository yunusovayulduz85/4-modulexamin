const createBtn = document.querySelector("#addBtn");
const input = document.querySelector("#inputTask");
const Task = document.querySelector("#Task");
const span=document.querySelector("#span");
let count=0;
createBtn.addEventListener("click", () => {
   
    if (input.value != "") {
        var div = document.createElement("div");
        const p = document.createElement("p");
        const span = document.createElement("span");
        const span2 = document.createElement("span");
        p.textContent = input.value;
        p.style.cursor="pointer"
        span.innerHTML = `<i onClick="editPost(this)" class="fas fa-edit"></i>`
        span2.innerHTML = `<i onClick="deletePost(this)" class="fas fa-trash-alt"></i>`
        div.appendChild(p);
        div.appendChild(span);
        div.appendChild(span2);
        // div.style.display = "flex";
        // div.style.alignItems = "center";
        // div.style.gap = "10px";
        div.classList.add("wrapper")
        Task.appendChild(div);
        let arr2=Array.from(Task.children);
        localStorage.setItem("state",arr2);
        input.value = "";
        let arr=Array.from(div.children);
        arr.forEach(item=>{
            item.addEventListener("click",()=>{
                div.style.textDecoration ="line-through";
            })
        })
    }
    if (count >= 1) {
        span.style.display = "none";
    }
    else {
        span.textContent="Task kiriting..";
        count++;
    }
})
function deletePost(e) {
    e.parentElement.parentElement.remove();
}
function editPost(e) {
    input.value = e.parentElement.previousElementSibling.innerHTML;
    e.parentElement.parentElement.remove();
}

